const express = require("express");
const router = express.Router();
const usertype = require("../controllers/UserType");
router.get("/", usertype.viewUserType);
router.get("/:id", usertype.findOne);
router.post("/", usertype.addUserType);
router.put("/:id", usertype.update);
router.delete("/:id", usertype.deleteUserType);
module.exports = router;
