const usertype = require("../models/UserType");

const addUserType = (req, res) => {
  usertype
    .create({
      UserType_Name: req.body.UserType_Name,
      Status: req.body.Status,
      CreatedBy: req.body.CreatedBy
    })
    .then(usertype => res.send(usertype))
    .catch(err => res.status(400).send(err));
};
const viewUserType = (req, res) => {
  usertype
    .findAll({ where: { DeletedBy: null } })
    .then(usertype => res.send(usertype))
    .catch(err => res.status(400).send(err));
};
// const GetAllEmployees = (req, res) => {
//   usertype
//     .findAll({
//       where: {
//         DeletedBy: null,
//         $or: [
//           {
//             UserType_ID: {
//               $eq: 3
//             }
//           },
//           {
//             UserType_ID: {
//               $eq: 6
//             }
//           }
//         ]
//       }
//     })
//     .then(usertype => res.send(usertype))
//     .catch(err => res.status(400).send(err));
// };

const findOne = (req, res) => {
  usertype
    .findOne({
      where: [{ UserType_ID: req.params.id }, { Status: true }]
    })
    .then(usertype => res.send(usertype))
    .catch(err => res.status(400).send(err));
};

const update = (req, res) => {
  usertype
    .update(
      {
        UserType_Name: req.body.UserType_Name,
        Status: req.body.Status,
        UpdatedBy: req.body.UpdatedBy,
        DeletedBy: req.body.DeletedBy,
        DeletedAt: req.body.DeletedAt
      },
      {
        where: { UserType_ID: req.params.id }
      }
    )
    .then(usertype => res.sendStatus(200))
    .catch(err => res.status(400).send(err));
};

const deleteUserType = (req, res) => {
  usertype
    .update(
      {
        Status: req.body.Status,
        DeletedBy: req.body.DeletedBy,
        DeletedAt: req.body.DeletedAt
      },
      {
        where: { UserType_ID: req.params.id }
      }
    )
    .then(usertype => res.sendStatus(200))
    .catch(err => res.status(400).send(err));
};

module.exports = {
  addUserType,
  deleteUserType,
  viewUserType,
  update,
  findOne
};
