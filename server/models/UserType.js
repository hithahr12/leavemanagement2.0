const Sequelize = require("sequelize");
const sequelize = require("../config/dbconfig");

const Model = Sequelize.Model;
class UserType extends Model {}
UserType.init(
  {
    UserType_ID: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    UserType_Name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "UserType Already Exists!"
      }
    },
    Status: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    CreatedBy: {
      type: Sequelize.BIGINT,
      allowNull: false
      // ,
      // references: {
      //   model: "cbl_users",
      //   key: "User_ID"
      // }
    },
    UpdatedBy: {
      type: Sequelize.BIGINT,
      allowNull: true
      // ,
      // references: {
      //   model: "cbl_users",
      //   key: "User_ID"
      // }
    },
    DeletedBy: {
      type: Sequelize.BIGINT,
      allowNull: true
      // ,
      // references: {
      //   model: "cbl_users",
      //   key: "User_ID"
      // }
    },
    DeletedAt: {
      type: Sequelize.DATE,
      allowNull: true
      // ,
      // references: {
      //   model: "cbl_users",
      //   key: "User_ID"
      // }
    }
  },
  {
    sequelize,
    modelName: "cbl_usertype"
    // options
  }
);

module.exports = UserType;
