const express = require("express");
const sequelize = require("./config/dbconfig");
const cors = require("cors");
const fileUpload = require("express-fileupload");
const path = require("path");
require("dotenv").config();

// Initalization
const app = express();
// Middlewares
app.use(cors());
// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json({ limit: "50mb", extended: true }));
app.use(fileUpload({ createParentPath: true, preserveExtension: true }));
app.use("/storage", express.static(path.join(__dirname, "storage")));

// Connecting To Database
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

sequelize.sync().then(() => console.log("Synced"));

// Routes
app.use("/userType", require("./routes/UserType"));
// Starting App
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server Connected To ${port}`));
